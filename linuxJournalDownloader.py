#!/data/ngi/soft/bin/python

import urllib2
from BeautifulSoup import BeautifulSoup as bsoup
from optparse import OptionParser

Months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
issue_type = ['pdf','pub','mobi']
#account number has the format AA123456"
accountNumber = ""
#zip format has the format 12345
zipCode = ""
url = "http://download.linuxjournal.com/pdf/dljdownload.php?"+"AN="+accountNumber+"&ZP="+zipCode
folder = "."

class Link(object):
    def __init__(self, type, link):
        self.__type = type
        self.__link = link

    def getLink(self):
        return self.__link

    def getLinkType(self):
        return self.__type

    def __str__(self):
        return "Link type: " + self.__type + " | link: " + self.__link

class LinuxJournalIssue(object):
    def __init__(self, date, issueFocus, links):
        self.__date = date
        self.__issueFocus = issueFocus
        #an issue contains more than one single link
        self.__link = links

    #this returns the link itself
    def getLinkByType(self, linkType):
        if len(self.__link) == 0:
            raise Exception('journalIssueException','List of links not existing')
        for link in self.__link:
            if link.getLinkType().lower() == linkType.lower():
		print "returned link %s" % link
                return link

    def getLinkList(self):
	return self.__link   
 

    def setLinkList(self, linkList):
        self.__link = linkList

    def appendLink(self, link):
        self.__link.append(link)

    def getDate(self):
	return self.__date

    def getFocus(self):
        return self.__issueFocus

    def __str__(self):
	text = "Issue date: " + self.__date + " | Issue Focus: " + self.__issueFocus
	#return print "Issue date: " + self.__date + " Issue Focus " + self.__date
        for l in self.__link:
	    text = text + "\n"
            text =  text +  l.__str__()
	return text

class htmlParser(object):
    def __init__(self, account_number, zip_code):
        #self.__targetLink = "http://download.linuxjournal.com/pdf/dljdownload.php?"+"AN="+account_number+"&ZP="+(str((int(zip_code)+1)))
        self.__targetLink = "http://download.linuxjournal.com/pdf/dljdownload.php?"+"AN="+account_number+"&ZP="+zip_code
	print self.__targetLink
        self.__journal = []

    def getListOfIssues(self):
        return self.__journal

    def filterIssueByMonth(self, month):
        filteredList = []
        for issue in self.__journal:
            #print issue
            #print "issue.find(month)", issue.getDate(),month,issue.getDate().find(month)
            if ((issue.getDate()).lower()).find(month.lower()) != -1:
                filteredList.append(issue)
        self.__journal = filteredList

    def filterIssueByContent(self, content):
        filteredList = []
        for issue in self.__journal:
            if ((issue.getFocus()).lower()).find(content.lower()) != -1:
                filteredList.append(issue)
        self.__journal = filteredList

    def filterIssueByType(self, ltype):
        filteredList = []
        for issue in self.__journal:
            linkList = []
            for link in issue.getLinkList():
                if ((link.getLinkType()).lower()).find(ltype.lower()) != -1:
                   linkList.append(link)
            issue.setLinkList(linkList)
        for issue in self.__journal:
            if len(issue.getLinkList())>0:
                filteredList.append(issue)
        self.__journal = filteredList 

    def parse(self):
	try:
            print "Fetching data"
            response = urllib2.urlopen(self.__targetLink)
            #return soup(response.read())
            soup = bsoup(response)
            #look for all the trs
            trs = soup.findAll('tr')
            #the TR represents the whole issue
            for tr in trs:
                #print "****** tr ******"
                #print tr
                #print "****** tr ******"
                tds = tr.findAll('td')
                date = ''
                issueFocus = ''
                listoflink = []
                for td in tds:
                    #print "****** td ******"
                    #print td
                    #print "****** td ******"
                    if td.get('nowrap') != None:
                        if td.string.strip(' \n0123456789') in Months:
                            date = td.string.strip(' \n')
                            #print "**** date %s ****" % date
                        else:
                            issueFocus = td.string.strip(' \n')
                            #print "*** else %s ***" % issueFocus  
                    elif (td.find('div') != None and td.find('div').get('class') == 'downloadbtn'):
                        links = td.findAll('a')
                        for link in links:
                            #print "*** link: %s ***" % link
                            listoflink.append(Link((link.find('img').get('alt')[9:]).lower(),link.get('href')))
                    else:
                        continue
                if (len(date) == 0 or len(issueFocus) == 0 or len(listoflink) == 0):
		    continue
                #print "%s %s %s" % (date, issueFocus, len(listoflink))
                issue = LinuxJournalIssue(date,issueFocus,listoflink)
                self.__journal.append(issue)
            if(len(self.__journal) == 0):
                raise Exception('AuthenticationException','Account number or zip code not valid, the page provided a empty resultset')
            print len(self.__journal)
            print "Data parsed correctly"
        except urllib2.HTTPError, e:
	    print "HTTP exception, the request ended up with code %s" % e.code
	except urllib2.URLError, e:
            print "#######\nURL error caught \n%s'\n#######" % e.args

    def saveAll(self,listOfTarget):
	for issue in issues:
            for l in issue.getLinkList():
                try:
                    print "Downloading issue %s | %s in %s format" % (issue.getDate(), issue.getFocus(), l.getLinkType())
                    remotefile = urllib2.urlopen(l.getLink()+"&action=spit")
                    data = remotefile.read()
                    remotefile.close()
                    filename = (issue.getDate()).replace(' ','_')+"_"+issue.getFocus().replace(' ','_')+"."+l.getLinkType()
                    if folder[-1] != '/':
                        folder += '/'
                    localfile=open(folder+filename,'wb')
                    print "Writing %s" % filename
                    localfile.write(data)
                    localfile.close
                except urllib2.HTTPError, e:
                    print "HTTP exception, the request ended up with code %s" % e.code
                except urllib2.URLError, e:
                    print "#######\nURL error caught \n%s'\n#######" % e.args
                except IOError, e:
                    print "I/O error({0}): {1}".format(e.errno, e.strerror)

    def listOfTarget(self):
        listOfTarget = []
        for issue in self.__journal:
            for link in issue.getLinkList():
                 listOfTarget.append({(issue.getDate()+"|"+issue.getFocus()):link})
        return listOfTarget

from threading import Thread
from threading import Semaphore
import time
class TDownloader(object):
    threadList = []
    def __init__(self, targets, threadNum):
        self.__targets = targets
        self.__threadLimiter = Semaphore(value=threadNum)

    def run(self):
        for target in self.__targets:
            for k,v in target.iteritems():
                try:
                    self.__threadLimiter.acquire()
                    t = Thread(target=TDownloader.download,args=(self, k, v, ))
                    TDownloader.threadList.append(t)
                    t.start()
                except(KeyboardInterrupt, SystemExit):
                    print '\n! Received keyboard interrupt, quitting threads.\n'
                    exit()
                

    def download(self, issue_details, link):
        try:
            month = issue_details[:(issue_details.find("|"))]
            focus = issue_details[(issue_details.find("|")+1):]
            print "Downloading issue %s-%s in %s format" % (month, focus, link.getLinkType())
            remotefile = urllib2.urlopen(link.getLink()+"&action=spit")
            data = remotefile.read()
            remotefile.close()
            filename = month.replace(' ','_')+"_"+focus.replace(' ','_')+'.'+link.getLinkType()
            global folder
            if folder[-1] != '/':
                folder += '/'
            print "Writing %s" % (folder+filename)
            localfile=open(folder+filename,'wb')
            localfile.write(data)
            localfile.close
            self.__threadLimiter.release()
        except urllib2.HTTPError, e:
            print "HTTP exception, the request ended up with code %s" % e.code
        except urllib2.URLError, e:
            print "#######\nURL error caught \n%s'\n#######" % e.args
        except IOError, e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)
        except(KeyboardInterrupt, SystemExit):
            print '\n! Received keyboard interrupt, quitting threads.\n'
            exit()

                
#MAIN
#parser = htmlParser(url)
#issues = parser.parse()
#parser.saveAll(issues)
parseOpt = OptionParser()
parseOpt.add_option('-l', '--list', help="list issues (list all by default)", dest="list_option", action="store_true", default=False)
parseOpt.add_option('-D', '--Download', help="download the targets with a given number of threads (default=3)", dest="download_target", action="store_true",default=False)
parseOpt.add_option('-d', '--date', help="filter the issues by month", dest="date_option")
parseOpt.add_option('-c', '--content', help="filter the issues content", dest="content_option")
parseOpt.add_option('-t', '--type', help="select the issue type, it might be pdf/mobi/epub, download all by default", dest="type_option")
parseOpt.add_option('-T', '--Threads', help="uses a given number of threads for the download (3 by default)", dest="thread_option")
parseOpt.add_option('--account_number', help="account number passed through command line", dest="account_number")
parseOpt.add_option('--zip_code', help="zip code passed through command line", dest="zip_code")
parseOpt.add_option('--folder', help="specifies a target directory", dest="dest_option")
options, remainder = parseOpt.parse_args()

print options.account_number, "|", options.zip_code
if options.account_number is not None:
    #fallbacks on the default values
    accountNumber = options.account_number
if options.zip_code is not None:
    zipCode = options.zip_code
if accountNumber == "":
    raise Exception('AccountNumberException','Account number not valid')
if zipCode == "":
    raise Exception('ZipCodeException','Zip code not valid')

#this parses the html page, might raise an exception because of the account number/zip code

if options.list_option == False and options.download_target == False:
    parseOpt.print_help()
    exit()
elif options.list_option == True and options.download_target == True:
    print "\n *** \n List option and Download option are exclusive \n ***"
    parseOpt.print_help()
    exit()
parser = htmlParser(accountNumber, zipCode)
try:
    parser.parse()
    #filter by date and content is permitted
    if options.date_option:
        #filters out whatever is not needed
        parser.filterIssueByMonth(options.date_option)
    if options.content_option:
        parser.filterIssueByContent(options.content_option)
    if options.type_option:
        parser.filterIssueByType(options.type_option)
    if options.list_option:
        for issue in parser.getListOfIssues():
            print issue
    elif options.download_target:
        number_of_threads = 3
        if options.thread_option:
            number_of_threads = int(options.thread_option)
        if options.dest_option:
           folder = options.dest_option
        print "Using", number_of_threads,"concurrent threads for the download"
        aThreadDownloader = TDownloader(parser.listOfTarget(),number_of_threads)
        aThreadDownloader.run()
except Exception as e:
     print e
